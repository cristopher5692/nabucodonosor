package com.core.repository;

import org.springframework.data.repository.CrudRepository;

import com.core.model.User;

public interface UserRepository extends CrudRepository<User, String>{

    @Override
    User findOne(String id);

    @Override
    void delete(User deleted);
    
}
