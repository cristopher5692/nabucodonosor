package com.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.core.model.Address;
import com.core.model.User;
import com.core.repository.UserRepository;

@Controller
public class UserController {

	@Autowired
	UserRepository userRepository;
	
	@RequestMapping("/users")
	public String users(Model model) {
		model.addAttribute("usersList", userRepository.findAll());
		return "users";
	}

	@RequestMapping("/users/{id}")
	public String user(@PathVariable String id, Model model) {
		model.addAttribute("user", userRepository.findOne(id));
		return "showUser";
	}
	

	@RequestMapping("/createUser")
	public String createUser(Model model) {
		return "createUser";
	}

	@RequestMapping("/saveUser")
	public String saveUser(
			@RequestParam String firstName,
			@RequestParam String lastName,
			@RequestParam String email,
			@RequestParam String type,
			@RequestParam String mobileNumber,
			@RequestParam String rol,
			/*@RequestParam Address address,*/
			@RequestParam String description) {
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setType(type);
		user.setMobileNumber(mobileNumber);
		user.setDescription(description);
		user.setRol(rol);
		//user.setAddress(address);
		userRepository.save(user);

		return "redirect:/users/" + user.getId();
		
	}
	
    @RequestMapping("/editUser/{id}")
    public String editUser(@PathVariable String id, Model model) {
        model.addAttribute("user", userRepository.findOne(id));
        return "editUser";
    }
    
    @RequestMapping("/updateUser")
    public String updateUser(@RequestParam String id, @RequestParam String firstName,
			@RequestParam String lastName,
			@RequestParam String email,
			@RequestParam String type,
			@RequestParam String mobileNumber,
			@RequestParam String rol,
			@RequestParam Address address,
			@RequestParam String description) {
		User user = userRepository.findOne(id);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setType(type);
		user.setMobileNumber(mobileNumber);
		user.setDescription(description);
		user.setRol(rol);
		user.setAddress(address);
		userRepository.save(user);
        return "redirect:/user/" + user.getId();
    }

    @RequestMapping("/deleteUser")
    public String deleteUser(@RequestParam String id) {
        User product = userRepository.findOne(id);
        userRepository.delete(product);

        return "redirect:/users";
    }
}
