package com.core.model;

import lombok.Data;

@Data
public class Address {

	private Long id;
	private String description;
}
