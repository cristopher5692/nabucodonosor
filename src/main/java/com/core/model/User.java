package com.core.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "user")
public class User {
	
	@Id
	private String id;
	private String firstName;
	private String lastName;
	private String email;
	private String type;
	private String mobileNumber;
	private String rol;
	private String description;
	private Address address;
	
}
