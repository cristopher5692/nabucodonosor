package com.core.model;

import lombok.Data;

@Data
public class Permission {
	
	private Long id;
	private String description;

}
