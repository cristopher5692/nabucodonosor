package com.core.model;

import lombok.Data;

@Data
public class Rol {
	
	private Long id;
	private String name;
	private Permission permission;
	

}
